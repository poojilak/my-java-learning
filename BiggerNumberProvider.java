import java.lang.*;
import java.util.*;

public class BiggerNumberProvider {

	public static void main(String[] args) 		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter first number :");
		String numStr1 = scanner.nextLine();
		int num1 = Integer.parseInt(numStr1);
		
		System.out.println("Enter second number :");
		String numStr2 = scanner.nextLine();
		int num2 = Integer.parseInt(numStr2);
		
		int sum = num1 + num2;

		System.out.println("Sum of "+ num1 + " and "+num2 + " = "+ sum);
}
