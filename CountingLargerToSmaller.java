import java.lang.*;
import java.util.*;

public class CountingLargerToSmaller {
	public static void main(String[] args){
			Scanner scanner = new Scanner(System.in);
			System.out.println("Enter the Larger Number:");
			String numStr1 = scanner.nextLine();
			int num1 = Integer.parseInt(numStr1);
			System.out.println("Enter the Smaller Number:");
			String numStr2 = scanner.nextLine();
			int num2 = Integer.parseInt(numStr2);
			int small,large;
			if (num1>num2) {
				large=num1;
				small=num2;
			}
			else{
				large=num2;
				small=num1;
			}
			for (int i=large;i>=small;i--){
				System.out.println(i);
			}
		}
}